// server.js
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');

var _ = require('lodash');
var moment = require('moment');

const mongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;
const uri = "mongodb+srv://cinema_user:Q10CCsdtuFCzWYF4@cluster0-gwmad.mongodb.net/test?retryWrites=true";

app.use(cors());
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
  console.log('We are live on ' + port);
});

app.get('/films', async (req, res) => {
  var films = [];
  mongoClient.connect("mongodb+srv://cinema_user:Q10CCsdtuFCzWYF4@cluster0-gwmad.mongodb.net/test?retryWrites=true", {
    useNewUrlParser: true
  }, (err, client) => {

    const db = client.db('cinema')
    const collection = db.collection("films");
    var cursor = collection.find({
      active: "yes"
    });
    cursor.forEach((doc, err) => {
      films.push(doc);
    }, () => {
      client.close();
      return res.send(JSON.stringify(films));
    })
    // perform actions on the collection object
  });
});

app.get('/premiers', async (req, res) => {
  var films = [];
  mongoClient.connect("mongodb+srv://cinema_user:Q10CCsdtuFCzWYF4@cluster0-gwmad.mongodb.net/test?retryWrites=true", {
    useNewUrlParser: true
  }, (err, client) => {

    const db = client.db('cinema')
    const collection = db.collection("films");
    var cursor = collection.find({
      active: "no"
    });
    cursor.forEach((doc, err) => {
      films.push(doc);
    }, () => {
      client.close();
      return res.send(JSON.stringify(films));
    })
    // perform actions on the collection object
  });
})

app.post('/order', async (req, res) => {
  mongoClient.connect("mongodb+srv://cinema_user:Q10CCsdtuFCzWYF4@cluster0-gwmad.mongodb.net/test?retryWrites=true", {
    useNewUrlParser: true
  }, (err, client) => {
    console.log(req.query.film_id)
    const db = client.db('cinema')
    const collection = db.collection("tikets");

    collection.insertOne({

    })

    var cursor = collection.find();
    cursor.forEach((doc, err) => {
      if (doc.film == req.query.film_id)
        tikets.push(doc);
    }, () => {
      client.close();
      return res.send(JSON.stringify(tikets));
    })
    // perform actions on the collection object
  });
})

app.get('/tickets', async (req, res) => {
  var tikets = [];
  mongoClient.connect("mongodb+srv://cinema_user:Q10CCsdtuFCzWYF4@cluster0-gwmad.mongodb.net/test?retryWrites=true", {
    useNewUrlParser: true
  }, (err, client) => {

    const db = client.db('cinema')
    const collection = db.collection("tikets");

    var cursor = collection.find({
      user: req.query.userId
    });
    cursor.forEach((doc, err) => {
      tikets.push(doc);
    }, () => {
      console.log('send result...');
      client.close();
      return res.send(JSON.stringify(tikets));
    })
    // perform actions on the collection object
  });
})

app.get('/screeningById', async (req, res) => {
  var screenings = [];
  mongoClient.connect("mongodb+srv://cinema_user:Q10CCsdtuFCzWYF4@cluster0-gwmad.mongodb.net/test?retryWrites=true", {
    useNewUrlParser: true
  }, (err, client) => {
    console.log("screan by id", req.query.id);
    if (req.query.id) {


      const db = client.db('cinema');
      const collection = db.collection("screening");

      var cursor = collection.find({
        _id: ObjectId(req.query.id)
      });
      cursor.forEach((doc, err) => {
        console.log('found!');
        screenings.push(doc);
      }, () => {
        client.close();
        return res.send(JSON.stringify(screenings));
      })
    } else {
      return res.send(JSON.stringify({
        error: "server error!"
      }));
    }
    // perform actions on the collection object
  });
})

app.post('/createTickets', async (req, res) => {
  var screenings = [];

  function getScreeningById(collection, id) {
    return new Promise((resolve, reject) => {
      var screening = collection.findOne({
        _id: ObjectId(id)
      });
      console.log("find screening", screening._id);
      resolve(screening);

    });
  }

  function getById(collection, id) {
    var item = collection.findOne({
      _id: ObjectId(id)
    });
    return (item);
  }

  function awaitWork(err, client) {
    console.log(req.body.id)
    const db = client.db('cinema');
    const collection = db.collection("screening");
    const tiketsColl = db.collection("tikets");
    const filmsColl = db.collection("films");
    console.log("lalala");

    var forUserInformation = "";
    var ticketsCount = 0;

    var data = req.body.rows;
    if (data.length > 0) {
      data.forEach(item => {
        item.seats.forEach(seat => {
          if (seat.state == "selected") {
            seat.state = "reserved";
            forUserInformation += "ряд:" + item.id + " место:" + seat.number + "\n";
            ticketsCount += 1;
          }
        })
      });
    }

    //update screaning
    collection.updateOne({
        _id: ObjectId(req.body.id)
      }, {
        $set: {
          rows: req.body.rows
        }
      },
      function (err, res) {
        if (err) throw err;
        console.log(res.result.nModified + " document(s) updated");
      });
      


    var film = null;
    var screaning = null
    getById(collection, req.body.id).then((scrRes) => {
      console.log("error", scrRes)

      screening = scrRes;

      getById(filmsColl, scrRes.film).then((filmRes) => {
        film = filmRes;
        console.log("seccess", filmRes);


        var tiket = {
          user: req.body.userId,
          screening: ObjectId(req.body.id),
          screeningDate: screening.date,
          screeningTime: screening.time,
          ticketsCount: ticketsCount.toString(),
          information: forUserInformation,
          filmName: film.name
        };
        console.log(tiket);
        tiketsColl.insertOne(tiket, function (err, result) {
          if (err) throw err;
          console.log("1 document inserted");
        });
      }).then(() => {
        client.close();
        return res.send(JSON.stringify(screening));
      });
    });
  }
  mongoClient.connect("mongodb+srv://cinema_user:Q10CCsdtuFCzWYF4@cluster0-gwmad.mongodb.net/test?retryWrites=true", {
    useNewUrlParser: true
  }, (err, client) => {
    awaitWork(err, client);
    // perform actions on the collection object
  });
})

app.get('/screening', async (req, res) => {
  var screenings = [];
  mongoClient.connect("mongodb+srv://cinema_user:Q10CCsdtuFCzWYF4@cluster0-gwmad.mongodb.net/test?retryWrites=true", {
    useNewUrlParser: true
  }, (err, client) => {
    console.log(req.query.film_id)
    const db = client.db('cinema')
    const collection = db.collection("screening");

    var cursor = collection.find({}, {
      rows: 0,
      room: 0
    });

    cursor.forEach((doc, err) => {
      if (doc.film == req.query.film_id)
        screenings.push(doc);
    }, () => {
      client.close();

      var group_by_date = _(screenings).groupBy(x => x.date).map((value, key) => ({
        date: key,
        array: _(value.map((item)=>{return({id:item._id,time:item.time})})).sortBy(k=>k.time)
      }));

      /*var group_by_date = _(screenings).groupBy(x => x.date).map((value, key) => ({
        id: value[0]._id,
        name: key,
        items: (_.map(_.map(value, 'time'), (strTime) => {
          return moment(strTime, "HH-mm").format("HH-mm")
        })).sort()
      })).value();*/

      return res.send(JSON.stringify(group_by_date));
    })
    // perform actions on the collection object
  });
})


app.get('/insert', async (req, res) => {
  mongoClient.connect("mongodb+srv://cinema_user:Q10CCsdtuFCzWYF4@cluster0-gwmad.mongodb.net/test?retryWrites=true", {
    useNewUrlParser: true
  }, (err, client) => {

    const collection = client.collection("screening");
    collection.insertOne({
      film: films.id,
      date: "26-05-2019",
      time: "11-20",
      room: [{
        id: "1",
        format: "imax 3d",
        rows: [{
          id: "1",
          seats: [{
            id: "1",
            number: "1",
            state: "empty"
          }]
        }]
      }]
    });

    var cursor = collection.find({
      active: "yes"
    });
    cursor.forEach((doc, err) => {
      films.push(doc);
    }, () => {
      client.close();
      return res.send(JSON.stringify(films));
    })
    // perform actions on the collection object
  });
})